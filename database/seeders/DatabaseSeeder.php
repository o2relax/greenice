<?php

namespace Database\Seeders;

use App\Http\Services\RequestStateMachine;
use App\Models\Currency;
use App\Models\Request;
use App\Models\User;
use App\Models\Wallet;
use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Currency::query()->insert(
            [
                ['name' => 'US Dollar', 'iso3' => 'USD', 'symbols' => '$'],
                ['name' => 'Euro', 'iso3' => 'EUR', 'symbols' => '€'],
                ['name' => 'Ukrainian hryvnia', 'iso3' => 'UAH', 'symbols' => '₴'],
            ]
        );
        $users = User::factory()->count(2)->create();
        /** @var Currency[] $currency */
        $currencies = Currency::all();

        /** @var User $user */
        foreach ($users as $user) {
            foreach ($currencies as $currency) {
                $user->wallets()->save(new Wallet(['currency_id' => $currency->getId(), 'amount' => random_int(10, 50)]));
            }
        }

        Request::query()->insert([
            [
                'state'            => RequestStateMachine::NEW,
                'seller_id'        => 1,
                'buyer_id'         => 2,
                'sale_amount'      => 10,
                'cost_amount'      => 10,
                'sale_currency_id' => 1,
                'cost_currency_id' => 2,
                'created_at'       => Carbon::now(),
                'updated_at'       => Carbon::now(),
            ]
        ]);
    }
}
