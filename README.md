# Task description

• Реализовать возможность обмена средствами между кошельками пользователей.
• С каждой транзакции брать комиссию 2% в пользу системы.
• Подготовить данные (seed) для демонстрации (несколько пользователей, кошельков и заявок)
• Работа с системой осуществляется только через REST API.

Example:

1) The user A has 2 wallets USD - 100, UAH - 500
2) The user B has 3 wallets USD - 10, UAH - 2500, EUR - 400
3) Any user has ability to create requests:
   The user A creates a request to sell 50 USD for 2000 UAH
4) Any user has ability to get listing of requests except own
   price for buyer: 2040 UAH (2000 + 2%), 40 UAH system fee
5) Any user who has ability, can apply request
6) After transaction user A wallets USD - 50, UAH - 2500, user B - USD - 60, UAH - 460, EUR - 400
7) The additional API endpoint: sum of system fee for date interval
   Example:
   Request : date_from = 2022-07-01 date_to = 2022-08-01
   Response [{currency: UAH, amount: 40}, {currency: USD, amount: 130}]

# Intro

- всю логику оставил в контроллере для уменьшения времязатрат
- не использовал дополнительных пакетов (типо трансформеров)
- сиды полуфабричные. реквесты не фабриковал, так как нужно считать чтобы на них денег хватало у юзеров
- все синхронно, без джоб и тд
- Так же не создавал прав доступа и ролей, общая комиссия смотрится по любому апи токену

# Env

- PHP 7.4, Mysql 5.7
- Laravel 8.0

# Deploy

1. Edit your `/.env` database section
2. RUN `php artisan migrate`
3. RUN `php artisan db:seed`
4. Edit `/http-client.env.json` for use examples in `/api.http`

# API Doc

All example requests in the `api.http` file

## Requests list

Excepted own requests

```http
GET /api/v1/requests?api_token=12345678901234567890123456789012
Content-Type: application/json
Accept: application/json
```

| Parameter   | Type | Description                                 |
|:------------| :--- |:--------------------------------------------|
| `api_token` | `string` | **Required**. See in databese after `php artisan db:seed` |

## Responses

```javascript
[
    {
        "id": 1,
        "sale": {
            "currency": "USD",
            "amount": 50
        },
        "cost": {
            "currency": "UAH",
            "amount": 1.1
        }
    }
]
```

## Wallets list

```http
GET /api/v1/wallets?api_token=12345678901234567890123456789012
Content-Type: application/json
Accept: application/json
```

| Parameter   | Type | Description                                 |
|:------------| :--- |:--------------------------------------------|
| `api_token` | `string` | **Required**. See in databese after `php artisan db:seed` |

## Responses

```javascript
[
    {
        "currency": "USD",
        "amount": -38
    },
    {
        "currency": "EUR",
        "amount": 15
    },
    {
        "currency": "UAH",
        "amount": 50
    }
]
```

## Create request

```http
POST {{host}}/api/v1/requests
Content-Type: application/json
Accept: application/json

{
    "sale_amount": 50,
    "cost_amount": 5,
    "sale_currency_id": 1,
    "cost_currency_id": 3,
    "api_token": "12345678901234567890123456789012"
}
```

| Parameter   | Type      | Description                               |
|:------------|:----------|:------------------------------------------|
| `api_token` | `string`  | **Required**. See in databese after `php artisan db:seed` |
| `sale_amount` | `numeric`  | **Required**. |
| `cost_amount` | `numeric` | **Required**. |
| `cost_currency_id` | `string`  | **Required**. 1 - USD, 2 - EUR, 3 - UAH |
| `cost_currency_id` | `string`  | **Required**. 1 - USD, 2 - EUR, 3 - UAH |

## Responses

```javascript
[
    {
        "id": 1
    }
]
```

## Accept request

```http
POST {{host}}/api/v1/requests/{request_id}/accept
Content-Type: application/json
Accept: application/json

{
    "api_token": "12345678901234567890123456789012"
}
```

| Parameter   | Type      | Description                               |
|:------------|:----------|:------------------------------------------|
| `api_token` | `string`  | **Required**. See in databese after `php artisan db:seed` |

## Responses

```javascript
[
    
]
```

## Commissions sums

```http
GET /api/v1/requests/commissions?api_token=12345678901234567890123456789012
Content-Type: application/json
Accept: application/json
```

| Parameter   | Type | Description                                               |
|:------------| :--- |:----------------------------------------------------------|
| `api_token` | `string` | **Required**. See in databese after `php artisan db:seed` |
| `date_from` | `string` | *Optional*. Date format YYYY-MM-DD                        |
| `date_to`   | `string` | *Optional*. Date format YYYY-MM-DD      |

## Responses

```javascript
[
    {
        "amount": 1.1,
        "currency": "UAH"
    }
]
```

## Status Codes

| Status Code | Description                                |
| :--- |:-------------------------------------------|
| 200 | `OK`                                       |
| 400 | `BAD REQUEST`                              |
| 422 | `UNPROCESSABLE ENTITYT (Validation error)` |
| 404 | `NOT FOUND`                                |
| 500 | `INTERNAL SERVER ERROR`                    |
