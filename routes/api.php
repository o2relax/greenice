<?php

use App\Http\Controllers\Api\RequestController;
use App\Http\Controllers\Api\WalletController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->group(static function() {
    Route::prefix('requests')->group(static function() {
        Route::get('/', [RequestController::class, 'getList']);
        Route::post('/', [RequestController::class, 'create']);
        Route::get('commissions', [RequestController::class, 'getSystemCommissionAmounts']);
        Route::post('{new_request}/accept', [RequestController::class, 'accept']);
    });
    Route::prefix('wallets')->group(static function() {
        Route::get('/', [WalletController::class, 'getList']);
    });
});
