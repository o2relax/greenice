<?php

namespace App\Models;

use App\Traits\Eloquent\Id;
use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    use Id;

    protected $fillable = [
        'name', 'iso3', 'symbols'
    ];

    public const CREATED_AT = null;
    public const UPDATED_AT = null;

    public function getId(): ?int
    {
        return $this->getAttributeValue('id');
    }

    public function getIso3(): string
    {
        return $this->getAttributeValue('iso3');
    }
}
