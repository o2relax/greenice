<?php

namespace App\Models;

use App\Traits\Eloquent\Id;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property Currency $currency
 */
class Wallet extends Model
{
    use SoftDeletes, Id;

    public const RELATION_CURRENCY = 'currency';
    public const RELATION_USER = 'user';

    protected $fillable = [
        'amount',
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function currency(): BelongsTo
    {
        return $this->belongsTo(Currency::class);
    }

    public function getAmount(): float
    {
        return (float)$this->getAttributeValue('amount');
    }

    public function decreaseAmount(float $value): void
    {
        $this->setAttribute('amount', round($this->getAmount() - $value, 4));
    }

    public function increaseAmount(float $value): void
    {
        $this->setAttribute('amount', round($this->getAmount() + $value, 4));
    }
}
