<?php

namespace App\Models;

use App\Traits\Eloquent\Id;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property Currency saleCurrency
 * @property Currency costCurrency
 * @property User  seller
 * @property User buyer
 */
class Request extends Model
{
    use HasFactory, Id;

    public const SYSTEM_PAYMENT_COMMISSION = 0.02;

    protected $fillable = [
        'seller_id',
        'buyer_id',
        'sale_amount',
        'cost_amount',
        'sale_currency_id',
        'cost_currency_id',
        'state',
    ];

    public function setState(string $state)
    {
        return $this->setAttribute('state', $state);
    }

    public function getState(): string
    {
        return $this->getAttributeValue('state');
    }

    public function getSaleAmount(): float
    {
        return $this->getAttributeValue('sale_amount');
    }

    public function getCostAmount(): float
    {
        return round($this->getAttributeValue('cost_amount') + ($this->getAttributeValue('cost_amount') * self::SYSTEM_PAYMENT_COMMISSION), 4);
    }

    public function seller(): BelongsTo
    {
        return $this->belongsTo(User::class, 'seller_id');
    }

    public function buyer(): BelongsTo
    {
        return $this->belongsTo(User::class, 'buyer_id');
    }

    public function saleCurrency(): BelongsTo
    {
        return $this->belongsTo(Currency::class, 'sale_currency_id');
    }

    public function costCurrency(): BelongsTo
    {
        return $this->belongsTo(Currency::class, 'cost_currency_id');
    }
}
