<?php

namespace App\Http\Services;

use App\Models\Request;

class RequestStateMachine
{
    public const NEW = 'new';
    public const FINISHED = 'finished';
    public const ACCEPT = 'accept';

    public const TRANSACTIONS = [
        self::ACCEPT         => [
            self::NEW, self::FINISHED
        ],
    ];

    public function can(Request $request, string $action): bool
    {
        return isset(self::TRANSACTIONS[$action][0]) && self::TRANSACTIONS[$action][0] === $request->getState();
    }

    public function apply(Request $request, string $action): void
    {
        if (\array_key_exists($action, self::TRANSACTIONS)) {
            $request->setState(self::TRANSACTIONS[$action][1]);
        }
    }

}
