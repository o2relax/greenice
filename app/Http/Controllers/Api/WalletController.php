<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Wallet;
use Illuminate\Http\JsonResponse;

class WalletController extends Controller
{
    public function getList(): JsonResponse
    {
        return response()->json(
            Wallet::query()
                ->whereBelongsTo(auth()->user())
                ->with(Wallet::RELATION_CURRENCY)
                ->get()
                ->transform(static function (Wallet $wallet) {
                    return [
                        'currency' => $wallet->currency->getIso3(),
                        'amount'   => $wallet->getAmount(),
                    ];
                })
        );
    }
}
