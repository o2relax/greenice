<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateRequestRequest;
use App\Http\Requests\GetCommissionAmountRequest;
use App\Http\Services\RequestStateMachine;
use App\Models\Request;
use App\Models\User;
use App\Models\Wallet;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class RequestController extends Controller
{
    public function getList(): JsonResponse
    {
        return response()->json(
            Request::query()
                ->where('seller_id', '!=', auth()->user()->getId())
                ->where('state', '=', RequestStateMachine::NEW)
                ->get()
                ->transform(static function (Request $request) {
                    return [
                        'id'            => $request->getId(),
                        'sale' => [
                            'currency' => $request->saleCurrency->getIso3(),
                            'amount' => $request->getSaleAmount(),
                        ],
                        'cost' => [
                            'currency' => $request->costCurrency->getIso3(),
                            'amount' => $request->getCostAmount(),
                        ],
                    ];
                })
        );
    }

    public function create(CreateRequestRequest $request): JsonResponse
    {
        /** @var User $user */
        $user = $request->user();
        /** @var Wallet $userWallet */
        $userWallet = $user->wallets()->where('currency_id', $request->getSaleCurrencyId())->first();

        if (null === $userWallet || $request->getSaleAmount() > $userWallet->getAmount()) {
            return response()->json(['message' => 'Insufficient funds'], Response::HTTP_BAD_REQUEST);
        }

        $entity = new Request;
        $entity->fill($request->validated());
        $entity->setState(RequestStateMachine::NEW);
        $entity->seller()->associate($user);
        $entity->save();

        $userWallet->decreaseAmount($request->getSaleAmount());
        $userWallet->save();

        return response()->json(['id' => $entity->getId()]);
    }

    public function accept(Request $newRequest, RequestStateMachine $stateMachine): JsonResponse
    {
        DB::beginTransaction();
        // TODO для блокировки нужно делать запрос. через модель не знаю как :)
        /** @var Request $request */
        $request = Request::query()->lockForUpdate()->find($newRequest->getId());

        if (false === $stateMachine->can($request, RequestStateMachine::ACCEPT)) {
            DB::rollBack();

            return response()->json(['message' => 'You can\'t accept this request'], Response::HTTP_BAD_REQUEST);
        }

        /** @var User $user */
        $user = auth()->user();
        /** @var Wallet $buyerGiveWallet */
        /** @var Wallet $buyerGetWallet */
        $buyerGiveWallet = $user->wallets()->where('currency_id', $request->costCurrency->getId())->first();
        $buyerGetWallet = $user->wallets()->where('currency_id', $request->saleCurrency->getId())->first();
        /** @var Wallet $sellerGetWallet */
        $sellerGetWallet = $request->seller->wallets()->where('currency_id', $request->costCurrency->getId())->first();

        if (null === $buyerGiveWallet || $request->getSaleAmount() > $buyerGiveWallet->getAmount()) {
            DB::rollBack();
            return response()->json(['message' => 'Insufficient funds'], Response::HTTP_BAD_REQUEST);
        }

        $stateMachine->apply($request, RequestStateMachine::ACCEPT);
        $request->buyer()->associate($user);
        $request->save();

        $sellerGetWallet->increaseAmount($request->getCostAmount());
        $sellerGetWallet->save();
        $buyerGetWallet->increaseAmount($request->getCostAmount());
        $buyerGetWallet->save();
        $buyerGiveWallet->decreaseAmount($request->getSaleAmount());
        $buyerGiveWallet->save();

        DB::commit();

        return response()->json();
    }

    public function getSystemCommissionAmounts(GetCommissionAmountRequest $request): JsonResponse
    {
        $builder = Request::query()
            ->selectRaw('SUM(cost_amount) * ' . Request::SYSTEM_PAYMENT_COMMISSION . ' as amount, currencies.iso3 as currency')
            ->join('currencies', 'currencies.id', '=', 'requests.cost_currency_id')
        ->where('state', '=', RequestStateMachine::FINISHED);
;
        if ($request->has('date_from')) {
            $builder->where('requests.created_at', '>=', Carbon::parse($request->input('date_from'))->startOfDay());
        }
        if ($request->has('date_to')) {
            $builder->where('requests.created_at', '<=', Carbon::parse($request->input('date_to'))->endOfDay());
        }

        return response()->json(
            $builder->groupBy('cost_currency_id')
                ->groupBy('currencies.iso3')
                ->get()
                ->transform(static function(Request $request) {
                    $request->setAttribute('amount', (float)$request->getAttributeValue('amount'));

                    return $request;
                })
                ->values()
        );
    }
}
