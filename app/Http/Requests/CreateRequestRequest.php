<?php

namespace App\Http\Requests;

use App\Models\Currency;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\App;
use Illuminate\Validation\Rule;

class CreateRequestRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $currency = App::make(Currency::class);

        return [
            'sale_amount'      => ['required','numeric'],
            'cost_amount'      => ['required','numeric'],
            'sale_currency_id' => [
                'required', 'numeric',
                Rule::exists(
                    $currency->getTable(),
                    $currency->getKeyName()
                )
            ],
            'cost_currency_id' => [
                'required', 'numeric',
                Rule::exists(
                    $currency->getTable(),
                    $currency->getKeyName()
                )
            ],
        ];
    }

    public function getSaleCurrencyId(): int
    {
        return (int)$this->input('sale_currency_id');
    }

    public function getSaleAmount(): float
    {
        return (float)$this->input('sale_amount');
    }
}
