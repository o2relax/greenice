<?php

namespace App\Traits\Eloquent;

trait Id
{
    public function getId(): ?int
    {
        return $this->getAttributeValue('id');
    }
}
